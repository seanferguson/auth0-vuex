module.exports = {
  auth0Client: 'CRAZY_KEY',
  auth0Domain: 'YOUR_DOMAIN.auth0.com',
  auth0Audience: 'YOUR_API_AUDIENCE',
  auth0Email: 'YOUR_AUTH0_USER_LOGIN',
  auth0Password: 'YOUR_AUTH0_USER_PASSWORD',
  auth0Nickname: 'YOUR_AUTH0_USER_NICKNAME'
}
