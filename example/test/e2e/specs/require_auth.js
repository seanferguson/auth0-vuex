// For authoring Nightwatch tests, see
// http://nightwatchjs.org/guide#usage

module.exports = {
  'Redirect when accessing page without login': function (browser) {
    const landingPage = browser.page.dashboard()
    landingPage.navigate()
    browser.assert.urlEquals(browser.page.landing().url());
    browser.end()
  },
}
