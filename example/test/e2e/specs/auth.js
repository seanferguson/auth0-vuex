let config = require('../../../../auth0config')

module.exports = {
  'Can log in': function (browser) {
    const landingPage = browser.page.landing()
    landingPage.navigate()
      .waitForElementVisible('@loggedOutMessage', 20000)
      .expect.element('@loggedOutMessage').to.be.visible
    const loginPage = landingPage.login()
    loginPage.waitForElementVisible("@email",20000)
    const dashboardPage = loginPage.login(config.auth0Email, config.auth0Password)

    dashboardPage.waitForElementVisible('@loggedInMessage', 2000)
      .expect.element('@loggedInMessage').to.be.visible

    dashboardPage.waitForElementVisible('@loggedInMessage', 2000)
      .expect.element('@loggedInMessage').text.to.contain(config.auth0Nickname)

    browser.end()
  }
}
