module.exports = {
  url: function() {
    return "https://seanferguson.auth0.com/login"
  },
  commands: [{
    login: function(email, password) {
      this.waitForElementVisible("@email", 2000)
        .setValue("@email", email)
        .waitForElementVisible("@password", 2000)
        .setValue("@password", password)
        .click("@submit")

      return this.api.page.dashboard()
    }
  }],
  elements: {
    email: { 
      selector: 'input[name=username]' 
    },
    password: { 
      selector: 'input[name=password]' 
    },
    submit: {
      selector: '.auth0-lock-submit'
    }
  }
};
