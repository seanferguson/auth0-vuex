module.exports = {
  url: function() {
    return this.api.launchUrl + "/home"
  },
  commands: [{
    login: function() {
      this.section.header.waitForElementVisible("@loginLink", 2000)
      this.section.header.click("@loginLink")
      return this.api.page.login()
    }
  }],
  sections: {
    header: {
      selector: ".t-header",
      elements: {
        loginLink: {
          selector: '.t-header-login'
        }
      }
    }

  },
  elements: {
    loggedInMessage: { 
      selector: '.t-logged-in' 
    },
  }
};
