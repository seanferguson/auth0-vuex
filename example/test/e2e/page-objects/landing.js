module.exports = {
  url: function() {
    return this.api.launchUrl + "/";
  },
  sections: {
    header: {
      selector: ".t-header",
      elements: {
        loginLink: {
          selector: '.t-header-login'
        }
      }
    }

  },
  elements: {
    loggedOutMessage: { 
      selector: '.t-logged-out' 
    },
  },
  commands: [{
    login: function() {
      this.section.header.waitForElementVisible("@loginLink", 2000)
      this.section.header.click("@loginLink")
      return this.api.page.login()
    }
  }],
};
