// 1. start the dev server using production config
process.env.NODE_ENV = 'testing'

let opts = process.argv.slice(2)
if (opts.indexOf('--config') === -1) {
  opts = opts.concat(['--config', 'test/e2e/nightwatch.conf.js'])
}
if (opts.indexOf('--env') === -1) {
  opts = opts.concat(['--env', 'chrome'])
}

const spawn = require('cross-spawn')
const runner = spawn('./node_modules/.bin/nightwatch', opts, { stdio: 'inherit' })

runner.on('exit', function (code) {
  if(process.env.ALWAYS_EXIT_0) {
    process.exit(0)
  } else {
  process.exit(code)
  }
})

runner.on('error', function (err) {
  throw err
})
