require('babel-register')
var config = require('../../config')

// http://nightwatchjs.org/gettingstarted#settings-file
module.exports = {
  src_folders: ['test/e2e/specs'],
  output_folder: 'test/e2e/reports',
  custom_assertions_path: ['test/e2e/custom-assertions'],
  page_objects_path: ['test/e2e/page-objects'],

  selenium: {
    start_process: false,
  },

  test_settings: {
    default: {
      selenium_port: 4444,
      selenium_host: 'selenium',
      silent: true,
      launchUrl: process.env.LAUNCH_URL || 'http://frontend',
    },

    chrome: {
      desiredCapabilities: {
        browserName: 'chrome',
        javascriptEnabled: true,
        acceptSslCerts: true
      }
    },
  }
}
