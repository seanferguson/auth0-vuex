import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import Landing from '@/components/Landing'
import {Component} from 'auth0-vuex'
import store from '@/store'

Vue.use(Router)

const router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Landing',
      component: Landing,
      meta: { noauth: true }
    },
    {
      path: '/home',
      name: 'Home',
      component: Home
    },
    {
      path: '/callback',
      name: 'Callback',
      component: Component,
      meta: { noauth: true }
    },
    {
      path: '*',
      redirect: '/'
    }
  ]
})

router.beforeEach((to, from, next) => {
  const authRequired = to.matched.some((route) => !route.meta.noauth)
  const authed = store.getters.user
  if (authRequired && !authed) {
    next('/')
  } else {
    next()
  }
})
export default router
