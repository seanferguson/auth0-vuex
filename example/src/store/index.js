import Vuex from 'vuex'
import Vue from 'vue'
import auth0 from 'auth0-vuex'
import createPersistedState from 'vuex-persistedstate'
import router from '@/router'
import config from '../../../auth0config'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    auth: auth0.store({
      auth0Options: {
        clientID: config.auth0Client,
        domain: config.auth0Domain,
        redirectUri: window.location.origin + '/callback',
        audience: config.auth0Audience
      },
      postLogoutUrl: window.location.origin,
      onNewAuth: function (authResult) {
        router.replace('/home')
      },
      onFailedAuth: function (err) {
        router.replace('/')
        console.log(err)
        alert(`Error: ${err.error}. Check the console for further details.`)
      }
    })
  },
  plugins: [createPersistedState()]
})
