FROM node
MAINTAINER Sean Ferguson <sean.ferguson.sf@gmail.com>

RUN yarn global add webpack webpack-cli

COPY ./package.json /app/
COPY ./yarn.lock /app/
WORKDIR /app
RUN yarn install

RUN mkdir /app/example
COPY example/package.json /app/example
COPY example/yarn.lock /app/example
WORKDIR /app/example
RUN yarn install

COPY . /app

WORKDIR /app/example
RUN yarn install
RUN yarn run docker-build

FROM nginx:alpine
MAINTAINER Sean Ferguson

COPY --from=0 /app/example/dist /usr/share/nginx/html
COPY example/nginx.conf /etc/nginx/nginx.conf
