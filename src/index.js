import storeFunc from './store.js';
import ComponentA from './Component.vue';

export const store = storeFunc
export const Component = ComponentA

export default {
  store,
  Component
}
