import auth0 from 'auth0-js'

function mergeAuth0Defaults(options) {
  return {
    responseType: 'token id_token',
    scope: 'openid profile email',
    ...options
  }
}

export default function(config) { 
  const auth = new auth0.WebAuth(mergeAuth0Defaults(config.auth0Options))

  var renewTimeout = null

  return {
    state: {
      auth: null
    },
      mutations: {
        login (state, payload) {
          state.auth = payload.auth
        },
          logout (state) {
            state.auth = null
          }
      },
      getters: {
        user (state) {
          if (state.auth) {
            return state.auth.idTokenPayload
          } else {
            return null
          }
        },
          token (state) {
            if (state.auth) {
              return state.auth.accessToken
            } else {
              return null
            }
          }
      },
      actions: {
        login (context) {
          auth.authorize()
        },
          logout (context) {
            context.commit('logout')
            auth.logout({
              returnTo: config.postLogoutUrl
            })
          },
          renewToken (context) {
            auth.checkSession({}, function (err, authResult) {
              context.dispatch('handleAuthResult', {err, authResult})
            })
          },
          handleAuthResult (context, {err, authResult}) {
            if (authResult && authResult.accessToken && authResult.idToken) {
              context.commit({
                type: 'login',
                auth: authResult
              })

              const millisUntilExp = authResult.idTokenPayload.exp * 1000 - (new Date()).getTime()
              renewTimeout = setTimeout(() => {
                context.dispatch('renewToken')
              }, millisUntilExp - 10000)

              config.onNewAuth(authResult)
            } else if (err) {
              config.onFailedAuth(err)
            }
          },
          handleAuthentication (context) {
            auth.parseHash((err, authResult) => {
              context.dispatch('handleAuthResult', {err, authResult})
            })
          }
      }
  }
}
