const webpack = require('webpack');
const merge = require('webpack-merge');
const path = require('path');
const VueLoaderPlugin = require('vue-loader/lib/plugin')

var config = {
  output: {
    path: path.resolve(__dirname + '/dist/'),
  },
  plugins : [
    new VueLoaderPlugin()
  ],
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /(node_modules|bower_components)/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['env', "stage-2"]
          }
        }
      },
      {
        test: /\.vue$/,
        use: 'vue-loader'
      },
      {
        test: /\.css$/,
        use: [
          "vue-style-loader",
          "css-loader"
        ]
      },
      {
        test: /\.svg$/,
        use: "svg-inline-loader"
      }
    ]
  },
  optimization: {
    minimize: true
  }
};


module.exports = [
  merge(config, {
    entry: path.resolve(__dirname + '/src/index.js'),
    output: {
      filename: 'auth0-vuex.min.js',
      libraryTarget: 'window',
      library: 'Auth0Vuex',
    }
  }),
  merge(config, {
    entry: path.resolve(__dirname + '/src/index.js'),
    output: {
      filename: 'auth0-vuex.js',
      libraryTarget: 'umd',
      library: 'autho-vuex',
      umdNamedDefine: true
    }
  })
];
